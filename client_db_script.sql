/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;



CREATE TABLE `action` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdat` datetime DEFAULT NULL,
  `details` varchar(50) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
   `ip_address` varchar(45) DEFAULT NULL,
  `agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKq7e8qgeirqpqadbcb2pqmsupi` (`user_id`),
  CONSTRAINT `FKq7e8qgeirqpqadbcb2pqmsupi` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `activity` (
  `begin_time` datetime NOT NULL,
  `device_id` int(11) NOT NULL,
  `activity_begin_location_lat` double DEFAULT NULL,
  `activity_begin_location_long` double DEFAULT NULL,
  `activity_driving_distance` double DEFAULT NULL,
  `activity_duration` bigint(20) DEFAULT NULL,
  `activity_end_location_lat` double DEFAULT NULL,
  `activity_end_location_long` double DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `stop_duration` bigint(20) DEFAULT NULL,
  `total_activity_duration` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`begin_time`,`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `alert` (
  `device_id` bigint(20) NOT NULL,
  `alert_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alert_type` varchar(45) DEFAULT NULL,
  `alert_location` varchar(255) DEFAULT NULL,
  `alert_referenced_value` longtext,
  `alert_value` varchar(255) DEFAULT NULL,
  `alert_lock` tinyint(1) DEFAULT NULL,
  `alert_seen` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`device_id`,`alert_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `alert_config` (
  `device_id` bigint(20) NOT NULL,
  `alert_config_type` varchar(45) NOT NULL,
  `alert_config_value` varchar(3000) NOT NULL,
  `alert_config_time` timestamp NULL DEFAULT NULL,
  `alert_config_lock` tinyint(1) DEFAULT NULL,
  `alert_config_actif` tinyint(1) DEFAULT NULL,
  `alert_config_name` varchar(45) DEFAULT NULL,
  `alert_config_poi_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`device_id`,`alert_config_type`,`alert_config_value`),
  KEY `FKh5gmkb0mam8xrgp0pvjp7fomo` (`alert_config_poi_id`),
  CONSTRAINT `FKh5gmkb0mam8xrgp0pvjp7fomo` FOREIGN KEY (`alert_config_poi_id`) REFERENCES `point_interest` (`point_interest_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `alertconfiguration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operator_id` int(11) DEFAULT NULL,
  `value1` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `value2` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `type` enum('SPEED','POI','INGNITION','RT','ZONE','TOW','INSURANCE','DRIVING_QUALITY','VISIT','ROAD_TAXES','OPERATIONAL_CERTIFICATION','PERMIT_CIRCULATION','DATE','CHARGER','Air_F','Engine_Oil','Fuel_F','Separator_F','Oil_F','Max_km','BATTERY_DURATION','BOARD_GAMES','DISK','PUMP','OIL','SIPHONAGE') CHARACTER SET utf8 DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `alert_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_useralert` (`alert_id`),
  KEY `FK_operator` (`operator_id`),
  CONSTRAINT `FK866n0lfam9y33ce62eet0t0ir` FOREIGN KEY (`alert_id`) REFERENCES `useralert` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `alertconfigurationoperator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `alert_configurations` (
  `alert_alert_id` bigint(20) NOT NULL,
  `configurations_configuration_id` bigint(20) NOT NULL,
  PRIMARY KEY (`alert_alert_id`,`configurations_configuration_id`),
  UNIQUE KEY `UK_erfiqiljwl2uixve05pmoo2el` (`configurations_configuration_id`),
  CONSTRAINT `FKdkbrjlgv1xsp3452a2xmjvisa` FOREIGN KEY (`alert_alert_id`) REFERENCES `alert_` (`alert_id`),
  CONSTRAINT `FKib82ss73b9bmk7lshcolkqlyf` FOREIGN KEY (`configurations_configuration_id`) REFERENCES `configuration` (`configuration_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `assignment` (
  `date` datetime NOT NULL,
  `driver_driver_id` bigint(20) NOT NULL,
  `vehicule_vehicule_id` bigint(20) NOT NULL,
  `active` bit(1) DEFAULT NULL,
  PRIMARY KEY (`date`,`driver_driver_id`,`vehicule_vehicule_id`),
  KEY `FKg2806vicq76d3wtu7b6prolr2` (`driver_driver_id`),
  KEY `FKpea0fybdv9kcp3wgpnywycbur` (`vehicule_vehicule_id`),
  CONSTRAINT `FKg2806vicq76d3wtu7b6prolr2` FOREIGN KEY (`driver_driver_id`) REFERENCES `driver` (`driver_id`),
  CONSTRAINT `FKpea0fybdv9kcp3wgpnywycbur` FOREIGN KEY (`vehicule_vehicule_id`) REFERENCES `vehicule` (`vehicule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `authority` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `battery_maintenance` (
  `amperage` bigint(20) DEFAULT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `number` bigint(20) DEFAULT NULL,
  `serie_number` varchar(255) DEFAULT NULL,
  `voltage` bigint(20) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `battery` BIT(1) DEFAULT NULL,
  `type_battery` BIGINT(20) DEFAULT NULL,
  `observations` VARCHAR(45) NULL,
  `odometre` BIGINT(20) NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  `prix_tva` int DEFAULT '0',
  `prix_htva` double DEFAULT '0',
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `FK6pu3h6iyiahle1reyivrmxs3` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `brake_maintenance` (
  `board_games` bit(1) DEFAULT NULL,
  `disk` bit(1) DEFAULT NULL,
  `oil` bit(1) DEFAULT NULL,
  `position` bit(1) DEFAULT NULL,
  `pump` bit(1) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `right_position` bit(1) DEFAULT NULL,
  `left_position` bit(1) DEFAULT NULL,
  `type_disk` VARCHAR(45) NULL DEFAULT NULL,
  `type_left_position` VARCHAR(45) DEFAULT NULL,
  `type_right_position` VARCHAR(45) DEFAULT NULL,
  `odometre` BIGINT(20) DEFAULT NULL,
  `next_odometre` BIGINT(20) NULL ,
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `FKawbwgtjqwms4h9ptyjy8mfaw1` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cal_alert` (
  `alert_id` bigint(20) NOT NULL DEFAULT '0',
  `last_id` bigint(20) DEFAULT '0',
  `last_time` timestamp NULL DEFAULT '2017-07-12 09:02:04',
  `last_state` varchar(45) DEFAULT 'nothingToReport',
  `last_reported_state` varchar(45) DEFAULT 'nothingToReport',
  `last_location_latitude` double DEFAULT '0',
  `last_location_longitude` double DEFAULT '0',
  `last_alert_begin_time` timestamp NULL DEFAULT '2017-07-12 09:02:04',
  `last_reported` tinyint(1) DEFAULT '0',
  `last_report_Alert` tinyint(1) DEFAULT '0',
  `last_alert_date` timestamp NULL DEFAULT '2017-07-12 09:02:04',
  `last_alert_location` varchar(255) DEFAULT 'NOTHING',
  `last_alert_referenced_value` varchar(5000) DEFAULT 'NOTHING',
  `last_alert_value` varchar(255) DEFAULT 'NOTHING',
  `last_alert_lock` tinyint(1) DEFAULT '0',
  `last_alert_type` varchar(45) DEFAULT 'NOTHING',
  `last_device_id` bigint(20) DEFAULT '0',
  `last_poi_id` bigint(20) NOT NULL DEFAULT '0',
  `previous_lat` double DEFAULT '0',
  `previous_lng` double DEFAULT '0',
  PRIMARY KEY (`alert_id`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

CREATE TABLE `cal_alert_commit` (
  `alert_id` bigint(20) NOT NULL DEFAULT '0',
  `last_id` bigint(20) DEFAULT '0',
  `last_time` timestamp NULL DEFAULT '2017-07-12 09:02:04',
  `last_state` varchar(45) DEFAULT 'nothingToReport',
  `last_reported_state` varchar(45) DEFAULT 'nothingToReport',
  `last_location_latitude` double DEFAULT '0',
  `last_location_longitude` double DEFAULT '0',
  `last_alert_begin_time` timestamp NULL DEFAULT '2017-07-12 09:02:04',
  `last_reported` tinyint(1) DEFAULT '0',
  `last_report_Alert` tinyint(1) DEFAULT '0',
  `last_alert_date` timestamp NULL DEFAULT '2017-07-12 09:02:04',
  `last_alert_location` varchar(255) DEFAULT 'NOTHING',
  `last_alert_referenced_value` varchar(5000) DEFAULT 'NOTHING',
  `last_alert_value` varchar(255) DEFAULT 'NOTHING',
  `last_alert_lock` tinyint(1) DEFAULT '0',
  `last_alert_type` varchar(45) DEFAULT 'NOTHING',
  `last_device_id` bigint(20) DEFAULT '0',
  `last_poi_id` bigint(20) NOT NULL DEFAULT '0',
  `previous_lat` double DEFAULT '0',
  `previous_lng` double DEFAULT '0',
  PRIMARY KEY (`alert_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cal_navette` (
  `device_id` bigint(20) NOT NULL,
  `time` timestamp NULL DEFAULT '2019-01-01 00:00:00',
  `to_dest` varchar(45) DEFAULT NULL,
  `odo_nav` double DEFAULT '0',
  `hour_nav` timestamp NULL DEFAULT '2019-01-01 00:00:00',
  `in_stop` int(11) DEFAULT '0',
  `in_serv` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `time_debut` timestamp NULL DEFAULT '2019-01-01 00:00:00',
  `start_odo` double DEFAULT '0',
  `start_tfu` double DEFAULT '0',
  `stop_counter` double DEFAULT '0',
  `number_nav` int(11) DEFAULT '0',
  PRIMARY KEY (`device_id`),
  CONSTRAINT `fk_cal_navette_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id_device`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cal_navette_commit` (
  `device_id` bigint(20) NOT NULL,
  `time` timestamp NULL DEFAULT '2019-01-01 00:00:00',
  `to_dest` varchar(45) DEFAULT NULL,
  `odo_nav` double DEFAULT '0',
  `hour_nav` timestamp NULL DEFAULT '2019-01-01 00:00:00',
  `in_stop` int(11) DEFAULT '0',
  `in_serv` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `time_debut` timestamp NULL DEFAULT '2019-01-01 00:00:00',
  `start_odo` double DEFAULT '0',
  `start_tfu` double DEFAULT '0',
  `stop_counter` double DEFAULT '0',
  `number_nav` int(11) DEFAULT '0',
  PRIMARY KEY (`device_id`),
  CONSTRAINT `fk_cal_navette_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id_device`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE `cal_option` (
  `device_id` bigint(10) NOT NULL,
  `last_id` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `last_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `last_FuelLevel` int(11) DEFAULT '-1',
  `last_Odo` double DEFAULT '0',
  `last_TFU` double DEFAULT '0',
  `consume_start_fuel_level` double DEFAULT '0',
  `consume_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `consume_start_odo` double DEFAULT '0',
  `consume_start_tfu` double DEFAULT '0',
  `appro_start_fuel_level` double DEFAULT '0',
  `appro_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `appro_start_odo` double DEFAULT '0',
  `appro_start_tfu` double DEFAULT '0',
  `appro_end_fuel_level` double DEFAULT '0',
  `appro_end_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `appro_end_odo` double DEFAULT '0',
  `appro_end_tfu` double DEFAULT '0',
  `val1` double DEFAULT '0',
  `val2` double DEFAULT '0',
  `val3` double DEFAULT '0',
  `val4` double DEFAULT '0',
  `val5` double DEFAULT '0',
  `activity_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat` double NOT NULL DEFAULT '0',
  `activity_start_lon` double NOT NULL DEFAULT '0',
  `activity_start_odo` double NOT NULL DEFAULT '0',
  `activity_start_tfu` double NOT NULL DEFAULT '0',
  `activity_in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `activity_daily_start_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_start_lat` double DEFAULT '0',
  `activity_daily_start_lon` double DEFAULT '0',
  `activity_daily_start_odo` double NOT NULL DEFAULT '0',
  `activity_daily_start_tfu` double NOT NULL DEFAULT '0',
  `activity_daily_state` int(11) NOT NULL DEFAULT '0',
  `activity_daily_cumul_working_time` double NOT NULL DEFAULT '0',
  `activity_daily_last_reported_date` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_start_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_start_lat` double DEFAULT '0',
  `activity_daily_path_start_lon` double DEFAULT '0',
  `activity_daily_path_start_odo` double NOT NULL DEFAULT '0',
  `activity_daily_path_start_tfu` double NOT NULL DEFAULT '0',
  `activity_daily_path_state` int(11) NOT NULL DEFAULT '0',
  `activity_daily_path_cumul_working_time` double NOT NULL DEFAULT '0',
  `activity_daily_path_last_reported_date` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_end_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_end_lat` double DEFAULT '0',
  `activity_daily_path_end_lon` double DEFAULT '0',
  `activity_daily_path_end_odo` double NOT NULL DEFAULT '0',
  `activity_daily_path_end_tfu` double NOT NULL DEFAULT '0',
  `activity_daily_end_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_end_lat` double DEFAULT '0',
  `activity_daily_end_lon` double DEFAULT '0',
  `activity_daily_end_odo` double NOT NULL DEFAULT '0',
  `activity_daily_end_tfu` double NOT NULL DEFAULT '0',
  `last_valid_lat` double NOT NULL DEFAULT '0',
  `last_valid_lon` double NOT NULL DEFAULT '0',
  `activity_start_fuel_level` int(11) NOT NULL DEFAULT '0',
  `activity_end_fuel_level` int(11) NOT NULL DEFAULT '0',
  `activity_start_time_rpm` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat_rpm` double NOT NULL DEFAULT '0',
  `activity_start_lon_rpm` double NOT NULL DEFAULT '0',
  `activity_start_odo_rpm` double NOT NULL DEFAULT '0',
  `activity_start_tfu_rpm` double NOT NULL DEFAULT '0',
  `activity_in_progress_rpm` tinyint(1) NOT NULL DEFAULT '0',
  `tfu_alg` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;


CREATE TABLE `cal_option_commit` (
  `device_id` bigint(10) NOT NULL,
  `last_id` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `last_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `last_FuelLevel` int(11) DEFAULT '-1',
  `last_Odo` double DEFAULT '0',
  `last_TFU` double DEFAULT '0',
  `consume_start_fuel_level` double DEFAULT '0',
  `consume_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `consume_start_odo` double DEFAULT '0',
  `consume_start_tfu` double DEFAULT '0',
  `appro_start_fuel_level` double DEFAULT '0',
  `appro_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `appro_start_odo` double DEFAULT '0',
  `appro_start_tfu` double DEFAULT '0',
  `appro_end_fuel_level` double DEFAULT '0',
  `appro_end_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `appro_end_odo` double DEFAULT '0',
  `appro_end_tfu` double DEFAULT '0',
  `val1` double DEFAULT '0',
  `val2` double DEFAULT '0',
  `val3` double DEFAULT '0',
  `val4` double DEFAULT '0',
  `val5` double DEFAULT '0',
  `activity_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat` double NOT NULL DEFAULT '0',
  `activity_start_lon` double NOT NULL DEFAULT '0',
  `activity_start_odo` double NOT NULL DEFAULT '0',
  `activity_start_tfu` double NOT NULL DEFAULT '0',
  `activity_in_progress` tinyint(1) NOT NULL DEFAULT '0',
  `activity_daily_start_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_start_lat` double DEFAULT '0',
  `activity_daily_start_lon` double DEFAULT '0',
  `activity_daily_start_odo` double NOT NULL DEFAULT '0',
  `activity_daily_start_tfu` double NOT NULL DEFAULT '0',
  `activity_daily_state` int(11) NOT NULL DEFAULT '0',
  `activity_daily_cumul_working_time` double NOT NULL DEFAULT '0',
  `activity_daily_last_reported_date` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_start_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_start_lat` double DEFAULT '0',
  `activity_daily_path_start_lon` double DEFAULT '0',
  `activity_daily_path_start_odo` double NOT NULL DEFAULT '0',
  `activity_daily_path_start_tfu` double NOT NULL DEFAULT '0',
  `activity_daily_path_state` int(11) NOT NULL DEFAULT '0',
  `activity_daily_path_cumul_working_time` double NOT NULL DEFAULT '0',
  `activity_daily_path_last_reported_date` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_end_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_path_end_lat` double DEFAULT '0',
  `activity_daily_path_end_lon` double DEFAULT '0',
  `activity_daily_path_end_odo` double NOT NULL DEFAULT '0',
  `activity_daily_path_end_tfu` double NOT NULL DEFAULT '0',
  `activity_daily_end_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_daily_end_lat` double DEFAULT '0',
  `activity_daily_end_lon` double DEFAULT '0',
  `activity_daily_end_odo` double NOT NULL DEFAULT '0',
  `activity_daily_end_tfu` double NOT NULL DEFAULT '0',
  `last_valid_lat` double NOT NULL DEFAULT '0',
  `last_valid_lon` double NOT NULL DEFAULT '0',
  `activity_start_fuel_level` int(11) NOT NULL DEFAULT '0',
  `activity_end_fuel_level` int(11) NOT NULL DEFAULT '0',
  `activity_start_time_rpm` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat_rpm` double NOT NULL DEFAULT '0',
  `activity_start_lon_rpm` double NOT NULL DEFAULT '0',
  `activity_start_odo_rpm` double NOT NULL DEFAULT '0',
  `activity_start_tfu_rpm` double NOT NULL DEFAULT '0',
  `activity_in_progress_rpm` tinyint(1) NOT NULL DEFAULT '0',
  `tfu_alg` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `cal_travel` (
  `device_id` bigint(20) NOT NULL,
  `last_id` bigint(20) NOT NULL DEFAULT '0',
  `state` int(11) NOT NULL DEFAULT '0',
  `last_time` timestamp NOT NULL DEFAULT '2010-01-01 00:00:00',
  `last_position_lat` double DEFAULT '0',
  `last_position_long` double DEFAULT '0',
  `last_path_start_time` timestamp NULL DEFAULT NULL,
  `last_start_position_lat` double DEFAULT '0',
  `last_start_position_long` double DEFAULT '0',
  `last_max_speed` int(4) DEFAULT '0',
  `last_distance_driven` double DEFAULT '0',
  `last_path_duration` bigint(20) DEFAULT '0',
  `doubt_end_path` timestamp NULL DEFAULT NULL,
  `report_start` int(1) DEFAULT '0',
  `last_stop_start_time` timestamp NULL DEFAULT NULL,
  `last_stop_position_lat` double DEFAULT '0',
  `last_stop_position_long` double DEFAULT '0',
  `last_stop_duration` bigint(20) NOT NULL DEFAULT '0',
  `doubt_end_stop` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `report_stop` int(1) DEFAULT '0',
  `last_mileage_distance_driven` double DEFAULT '0',
  `last_mileage_duration` bigint(20) DEFAULT '0',
  `last_mileage_start_time` timestamp NULL DEFAULT NULL,
  `last_mileage_end_time` timestamp NULL DEFAULT NULL,
  `last_activity_begin_time` timestamp NULL DEFAULT NULL,
  `last_activity_end_time` timestamp NULL DEFAULT NULL,
  `last_total_activity_duration` bigint(20) DEFAULT NULL,
  `last_activity_duration` bigint(20) DEFAULT NULL,
  `last_activity_driving_distance` double DEFAULT NULL,
  `last_activity_begin_location_lat` double DEFAULT '0',
  `last_activity_begin_location_long` double DEFAULT '0',
  `last_activity_stop_duration` bigint(20) DEFAULT NULL,
  `report_activity` int(1) DEFAULT NULL,
  `was_Ignition` int(1) DEFAULT NULL,
  `last_activity_end_location_lat` double DEFAULT NULL,
  `last_activity_end_location_long` double DEFAULT NULL,
  `accum_odo` double DEFAULT '0',
  `last_path_start_odo` double NOT NULL DEFAULT '0',
  `last_path_start_tfu` double NOT NULL DEFAULT '0',
  `last_overspeed_begin_lat` double DEFAULT '0',
  `last_overspeed_begin_lon` double DEFAULT NULL,
  `last_overspeed_begin_time` timestamp NULL DEFAULT NULL,
  `last_overspeed_max_speed` int(4) DEFAULT NULL,
  `last_mileage_start_odo` double NOT NULL DEFAULT '0',
  `last_mileage_start_tfu` double NOT NULL DEFAULT '0',
  `accum_tfu` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`),
  KEY `FKdgf843sp9ammorctn7u2l564r` (`device_id`)
) ENGINE=MEMORY DEFAULT CHARSET=latin1;

CREATE TABLE `cal_travel_commit` (
  `device_id` bigint(20) NOT NULL,
  `last_id` bigint(20) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  `last_time` timestamp NOT NULL DEFAULT '2010-01-01 00:00:00',
  `last_position_lat` double DEFAULT '0',
  `last_position_long` double DEFAULT '0',
  `last_path_start_time` timestamp NULL DEFAULT NULL,
  `last_start_position_lat` double DEFAULT '0',
  `last_start_position_long` double DEFAULT '0',
  `last_max_speed` int(4) DEFAULT '0',
  `last_distance_driven` double DEFAULT '0',
  `last_path_duration` bigint(20) DEFAULT '0',
  `doubt_end_path` timestamp NULL DEFAULT NULL,
  `report_start` int(1) DEFAULT '0',
  `last_stop_start_time` timestamp NULL DEFAULT NULL,
  `last_stop_position_lat` double DEFAULT '0',
  `last_stop_position_long` double DEFAULT '0',
  `last_stop_duration` bigint(20) DEFAULT '0',
   `doubt_end_stop` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `report_stop` int(1) DEFAULT '0',
  `last_mileage_distance_driven` double DEFAULT '0',
  `last_mileage_duration` bigint(20) DEFAULT '0',
  `last_mileage_start_time` timestamp NULL DEFAULT NULL,
  `last_mileage_end_time` timestamp NULL DEFAULT NULL,
  `last_activity_begin_time` timestamp NULL DEFAULT NULL,
  `last_activity_end_time` timestamp NULL DEFAULT NULL,
  `last_total_activity_duration` bigint(20) DEFAULT NULL,
  `last_activity_duration` bigint(20) DEFAULT NULL,
  `last_activity_driving_distance` double DEFAULT NULL,
  `last_activity_begin_location_lat` double DEFAULT '0',
  `last_activity_begin_location_long` double DEFAULT '0',
  `last_activity_stop_duration` bigint(20) DEFAULT NULL,
  `report_activity` int(1) DEFAULT NULL,
  `was_Ignition` int(1) DEFAULT NULL,
  `last_activity_end_location_lat` double DEFAULT NULL,
  `last_activity_end_location_long` double DEFAULT NULL,
  `accum_odo` double DEFAULT '0',
  `last_path_start_odo` double NOT NULL DEFAULT '0',
  `last_path_start_tfu` double NOT NULL DEFAULT '0',
  `last_overspeed_begin_lat` double DEFAULT '0',
  `last_overspeed_begin_lon` double DEFAULT NULL,
  `last_overspeed_begin_time` timestamp NULL DEFAULT NULL,
  `last_overspeed_max_speed` int(4) DEFAULT NULL,
  `last_mileage_start_odo` DOUBLE NOT NULL DEFAULT '0' ,
 `last_mileage_start_tfu` DOUBLE NOT NULL DEFAULT '0' ,
`accum_tfu` DOUBLE NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`device_id`),
  KEY `FKdgf843sp9ammorctn7u2l564r` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `collect_setting` (
  `id_collect_setting` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_collect_setting` varchar(255) DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_collect_setting`),
  KEY `FK7c3b31us2foyjhob6biojh2jg` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `configuration` (
  `configuration_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) DEFAULT NULL,
  `information_id` bigint(20) DEFAULT NULL,
  `operator_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`configuration_id`),
  KEY `FKmmbmwoa86pporgwrh6l3cypt5` (`information_id`),
  KEY `FK78ji38trvced3c6mbji929mkm` (`operator_id`),
  CONSTRAINT `FK78ji38trvced3c6mbji929mkm` FOREIGN KEY (`operator_id`) REFERENCES `operator` (`operator_id`),
  CONSTRAINT `FKmmbmwoa86pporgwrh6l3cypt5` FOREIGN KEY (`information_id`) REFERENCES `information` (`information_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `device` (
  `id_device` bigint(20) NOT NULL,
  `vehicule_id` bigint(20) DEFAULT NULL,
  `realtimerecord_id` bigint(20) DEFAULT NULL,
  `device_number` bigint(20) DEFAULT NULL,
  `rawstream_id` int(11) NOT NULL DEFAULT '0',
  `stream_id` varchar(45) DEFAULT NULL,
  `imei` varchar(20) DEFAULT NULL,
  `icc` varchar(20) DEFAULT NULL,
  `serial` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_device`),
  KEY `FK7iubld493jmj6xk3x2pems53x` (`realtimerecord_id`),
  KEY `FKchqmtj446ldwskbobvbano9rp` (`vehicule_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `device_carb` (
  `device_id` bigint(20) NOT NULL,
  `max` int(11) DEFAULT '100',
  `min` int(11) DEFAULT '0',
  `filtrage` int(11) DEFAULT '4',
  `volume` int(11) DEFAULT '100',
  `fuel_src` int(11) DEFAULT '0',
  `fuel_algo` int(11) DEFAULT '0',
  `fuel_average` double DEFAULT '0',
  `tfu_enabled` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`device_id`),
  KEY `FKdgf843sp9ammorctn7u2l564r` (`device_id`),
  CONSTRAINT `fk_device_carb_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id_device`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `device_mems` (
  `device_id` bigint(20) NOT NULL,
  `nb_frein` double DEFAULT '0.5',
  `nb_acc` double DEFAULT '0.5',
  `nb_drift` double DEFAULT '0.5',
  `nb_jerk` double DEFAULT '0.5',
  PRIMARY KEY (`device_id`),
  CONSTRAINT `fk_device_mems_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id_device`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `device_opt` (
  `device_id` bigint(20) NOT NULL,
  `use_ignition` int(1) DEFAULT '1',
  `use_fuel` int(1) DEFAULT '0',
  `use_temp` int(1) DEFAULT '0',
  `use_fms` int(1) DEFAULT '0',
  `use_j1708` int(1) DEFAULT '0',
  `use_id_driver` int(1) DEFAULT '0',
  `use_stop` int(1) DEFAULT '0',
  `use_mems` int(1) DEFAULT '0',
  `use_door` int(1) DEFAULT '0',
  `use_door2` int(1) DEFAULT '0',
  PRIMARY KEY (`device_id`),
  KEY `FKdgf843sp9ammorctn7u2l564r` (`device_id`),
  CONSTRAINT `fk_device_opt_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id_device`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `device_temp` (
  `device_id` bigint(20) NOT NULL,
  `threshold_high_enable` tinyint(1) DEFAULT '0',
  `threshold_high_value` int(11) DEFAULT '0',
  `threshold_low_enable` tinyint(1) DEFAULT '0',
  `threshold_low_value` int(11) DEFAULT '0',
  `type` enum('DIGITAL','ANALOG') DEFAULT 'DIGITAL',
  PRIMARY KEY (`device_id`),
  CONSTRAINT `fk_device_temp_1` FOREIGN KEY (`device_id`) REFERENCES `device` (`id_device`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `distribution_maintenance` (
  `date_buy` datetime DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `odometre` double DEFAULT '0',
  `cost` double DEFAULT '0',
  `mark` varchar(45) DEFAULT NULL,
  `next_odometre` double DEFAULT '0',
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  KEY `fk_distribution_maintenance_1_idx` (`id_maintenance`),
  CONSTRAINT `fk_distribution_maintenance_1` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `document` (
  `doc_type` varchar(31) NOT NULL,
  `id_document` bigint(20) NOT NULL AUTO_INCREMENT,
  `cost` double DEFAULT NULL,
  `date_operation` datetime DEFAULT NULL,
  `provider_id` bigint(20) DEFAULT NULL,
  `vehicule_id` bigint(20) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_document`),
  KEY `FK9ep9a9o6nonqb98qu02mot7ys` (`provider_id`),
  KEY `FK29ktopfp8sk6c0o6qdvvhbiul` (`vehicule_id`),
  CONSTRAINT `FK29ktopfp8sk6c0o6qdvvhbiul` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicule` (`vehicule_id`),
  CONSTRAINT `FK9ep9a9o6nonqb98qu02mot7ys` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id_provider`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `driver` (
  `driver_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cin` varchar(50) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `blood_group` varchar(50) DEFAULT NULL,
  `have_avatar` tinyint(1) DEFAULT '0',
  `rang_class` varchar(255) DEFAULT NULL,
  `driver_medically` tinyint(1) NOT NULL DEFAULT '0',
  `driving_training` tinyint(1) NOT NULL DEFAULT '0',
  `driving_safe` tinyint(1) NOT NULL DEFAULT '0',
  `intervention_sites` tinyint(1) NOT NULL DEFAULT '0',
  `driver_authorized` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`driver_id`),
  UNIQUE KEY `UK_fchuyotq64tagkwktlh4qttyy` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `driver_poi_client` (
  `driver_id` bigint(20) NOT NULL,
  `point_client_id` bigint(20) NOT NULL,
  PRIMARY KEY (`driver_id`,`point_client_id`),
  KEY `fk_driver_poi_client_2_idx` (`point_client_id`),
  CONSTRAINT `fk_driver_poi_client_1` FOREIGN KEY (`driver_id`) REFERENCES `driver` (`driver_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_driver_poi_client_2` FOREIGN KEY (`point_client_id`) REFERENCES `point_client` (`point_client_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `driver_mission` (
  `driver_id` bigint(20) NOT NULL,
  `mission_id` bigint(20) NOT NULL,
  PRIMARY KEY (`driver_id`,`mission_id`),
  KEY `fk_driver_mission_2_idx` (`mission_id`),
  CONSTRAINT `fk_driver_mission_1` FOREIGN KEY (`driver_id`) REFERENCES `driver` (`driver_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `embrayage_maintenance` (
  `date_buy` datetime DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `odometre` double DEFAULT '0',
  `cost` double DEFAULT '0',
  `mark` varchar(45) DEFAULT NULL,
  `next_odometre` double DEFAULT '0',
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  KEY `fk_embrayage_maintenance_1_idx` (`id_maintenance`),
  CONSTRAINT `fk_embrayage_maintenance_1` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `fiche_vehicule` (
  `id_vehicule` bigint(20) NOT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `power_hp` int(11) DEFAULT NULL,
  `seating` int(11) DEFAULT NULL,
  `fuel` enum('BENZIN','DIESEL','GAZ') DEFAULT 'BENZIN',
  `load_capacity` int(11) DEFAULT NULL,
  `ptac` int(11) DEFAULT NULL,
  `pv` int(11) DEFAULT NULL,
  `payload` int(11) DEFAULT NULL,
  `fuel_tank_capacity` int(11) DEFAULT NULL,
  `id_fuel_provider` bigint(20) DEFAULT NULL,
  `fuel_card_nr` int(11) DEFAULT NULL,
  `fuel_card_pin` int(11) DEFAULT NULL,
  `fuel_control_module` enum('YES','NO') DEFAULT 'YES',
  `control_magnetic_driver` enum('YES','NO') DEFAULT 'YES',
  `first_installation` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_change` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `soumission_distributed_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `soumission_available_till` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `restriction` varchar(100) DEFAULT NULL,
  `comment` varchar(100) DEFAULT NULL,
  `date_purchase` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `place_purshace` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_vehicule`),
  KEY `fk_fiche_vehicule_2_idx` (`id_fuel_provider`),
  CONSTRAINT `fk_fiche_vehicule_1` FOREIGN KEY (`id_vehicule`) REFERENCES `vehicule` (`vehicule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_fiche_vehicule_2` FOREIGN KEY (`id_fuel_provider`) REFERENCES `provider` (`id_provider`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `fueling` (
  `cost_unit` double DEFAULT NULL,
  `number_payment` bigint(20) DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `odometre` bigint(20) DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `id_document` bigint(20) NOT NULL,
  `payment_type_id` bigint(20) DEFAULT NULL,
  `cost_total` double DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `num_cart_fuel` varchar(255) DEFAULT NULL,
  `consommation` double DEFAULT NULL,
  `lieu` varchar(255) DEFAULT NULL,
  `signification` varchar(255) DEFAULT 'KM',
  `reference` varchar(225) DEFAULT NULL,
  `verify` tinyint(1) DEFAULT '0',
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_document`),
  KEY `FKc5umqvld9dr2udmhhpvrm16hv` (`payment_type_id`),
  CONSTRAINT `FKc5umqvld9dr2udmhhpvrm16hv` FOREIGN KEY (`payment_type_id`) REFERENCES `type` (`id_type`),
  CONSTRAINT `FKgrxhtkq1pf59nj252exh5ih4d` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `fueling_monthly` (
  `id_document` bigint(20) NOT NULL,
  `odometer` double NOT NULL,
  `distance` double NOT NULL,
  `observation` varbinary(255) DEFAULT NULL,
  `cost_l` double NOT NULL,
  PRIMARY KEY (`id_document`),
  KEY `fk_fueling_monthly_1_idx` (`id_document`),
  CONSTRAINT `fk_fueling_monthly_1` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `groupe` (
  `groupe_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url_image` varchar(100) DEFAULT NULL,
  `nom` varchar(50) NOT NULL,
  `enable_global_alert` tinyint(1) DEFAULT '0',
  `father_id` bigint(20) DEFAULT NULL,
  `report_is_invisible`  tinyint(1) DEFAULT '0',
  PRIMARY KEY (`groupe_id`),
  UNIQUE KEY `UK_b30fmcjbp89j49uc6qtdb4dni` (`nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `group_poi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `marker_url` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `group_point_client` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 DEFAULT NULL,
  `marker_url` varchar(45) DEFAULT NULL,
  `zone_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `icon_transport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_url` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `information` (
  `information_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `icon_url` varchar(255) DEFAULT NULL,
  `information_type` varchar(255) DEFAULT NULL,
  `label` varchar(50) DEFAULT NULL,
  `symbol` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`information_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `information_operator` (
  `information_id` bigint(20) NOT NULL,
  `operator_id` bigint(20) NOT NULL,
  PRIMARY KEY (`information_id`,`operator_id`),
  KEY `FK3vrafpslk7datgnv82yvtmht6` (`operator_id`),
  CONSTRAINT `FK3vrafpslk7datgnv82yvtmht6` FOREIGN KEY (`operator_id`) REFERENCES `operator` (`operator_id`),
  CONSTRAINT `FK88kta75amrjk6vhghsak0ofh6` FOREIGN KEY (`information_id`) REFERENCES `information` (`information_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `information_suggestions` (
  `information_information_id` bigint(20) NOT NULL,
  `suggestions_suggestion_id` bigint(20) NOT NULL,
  PRIMARY KEY (`information_information_id`,`suggestions_suggestion_id`),
  UNIQUE KEY `UK_4itbdoxkhneclwwulqh8k9b73` (`suggestions_suggestion_id`),
  CONSTRAINT `FKb98jt1dxr6p4c26fiajfc9vhb` FOREIGN KEY (`suggestions_suggestion_id`) REFERENCES `suggestion` (`suggestion_id`),
  CONSTRAINT `FKp8fckph6ylkp9bbaq54l63alm` FOREIGN KEY (`information_information_id`) REFERENCES `information` (`information_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `intervention` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `content` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_boitiers` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `request` longtext,
  `response` longtext,
  `submit_at` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `leasing` (
  `interet` double DEFAULT NULL,
  `loyer_ht` double DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `id_document` bigint(20) NOT NULL,
  `loyer_ttc` DOUBLE DEFAULT NULL,
  `status` VARCHAR(45) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_document`),
  CONSTRAINT `FKchy5qxyjktm2i1fccbyl165sg` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `location` (
  `date_fin` datetime DEFAULT NULL,
  `montant_caution` double DEFAULT NULL,
  `num_contrat` bigint(20) DEFAULT NULL,
  `odometre` bigint(20) DEFAULT NULL,
  `id_document` bigint(20) NOT NULL,
  `montant_ht` DOUBLE NULL DEFAULT NULL,
  `nom_conducteur1` VARCHAR(45) DEFAULT NULL,
  `nom_conducteur2` VARCHAR(45) DEFAULT NULL,
  `observation` VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (`id_document`),
  CONSTRAINT `FKoi44o2hlfafm40d2t3cxlj8a5` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `maintenance` (
  `maintenance_type` varchar(31) NOT NULL,
  `id_maintenance` bigint(20) NOT NULL AUTO_INCREMENT,
  `cost` double DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `date_operation` datetime DEFAULT NULL,
  `provider_id` bigint(20) DEFAULT NULL,
  `vehicule_id` bigint(20) DEFAULT NULL,
  `labor_cost` double DEFAULT NULL,
  `reference_da_dr` varchar(45) DEFAULT NULL,
  `actual_repair_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  KEY `FKx7j5n055fa64ygw792gt1ono` (`provider_id`),
  KEY `FKprlafcxlm7a76e8mce92tuh8i` (`vehicule_id`),
  CONSTRAINT `FKprlafcxlm7a76e8mce92tuh8i` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicule` (`vehicule_id`),
  CONSTRAINT `FKx7j5n055fa64ygw792gt1ono` FOREIGN KEY (`provider_id`) REFERENCES `provider` (`id_provider`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `mileage` (
  `device_id` int(11) NOT NULL,
  `start_hour` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `end_hour` timestamp NULL DEFAULT NULL,
  `driving_distance` double DEFAULT '0',
  `driving_duration` bigint(20) DEFAULT '0',
  `start_lat` DOUBLE NOT NULL DEFAULT '0' ,
 `start_lon` DOUBLE NOT NULL DEFAULT '0' ,
  `start_odo` DOUBLE NOT NULL DEFAULT '0' ,
  `start_tfu` DOUBLE NOT NULL DEFAULT '0' ,
  `end_lat` DOUBLE NOT NULL DEFAULT '0' ,
  `end_lon` DOUBLE NOT NULL DEFAULT '0' ,
  `end_odo` DOUBLE NOT NULL DEFAULT '0' ,
  `end_tfu` DOUBLE NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`device_id`,`start_hour`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `mission` (
  `mission_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) DEFAULT NULL,
  `served` bit(1) NOT NULL,
  `travel_distance` double DEFAULT NULL,
  `travel_duration` double DEFAULT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type_fuel` tinyint(1) DEFAULT '0',
  `fuel` double DEFAULT '0',
  `driver_id` bigint(20) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `description_payload` varchar(45) DEFAULT NULL,
  `date_depart` timestamp NULL DEFAULT NULL,
  `date_end` timestamp NULL DEFAULT NULL,
  `vehicule_id` bigint(20) DEFAULT NULL,
  `matricule` varchar(45) DEFAULT NULL,
  `accompaniement` varchar(45) DEFAULT NULL,
  `distance` double DEFAULT '0',
  `lieu_depart` varchar(255) DEFAULT NULL,
  `observation` varchar(255) DEFAULT NULL,
  `odometre` double DEFAULT NULL,
  `representing_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`mission_id`),
  KEY `fk_mission_1_idx` (`driver_id`),
  CONSTRAINT `fk_mission_1` FOREIGN KEY (`driver_id`) REFERENCES `driver` (`driver_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `navette` (
  `navette_id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(45) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL,
  `km_nav` double NOT NULL DEFAULT '0',
  `group_poi_client_id` bigint(20) NULL DEFAULT '0',
    `start_cron` varchar(45) DEFAULT NULL,
  `end_cron` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`navette_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `notification` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `alert_value` varchar(255) DEFAULT NULL,
  `alertid` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  `is_readed` tinyint(4) NOT NULL DEFAULT '0',
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `poi_id` varchar(255) DEFAULT NULL,
  `id_document` bigint(20) DEFAULT NULL,
  `message` varchar(2000) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `fuel_rate` double NOT NULL DEFAULT '0',
  `ignition` tinyint(1) DEFAULT NULL,
  `odo` double NOT NULL DEFAULT '0',
  `tfu` double NOT NULL DEFAULT '0',
  `temp_engine` int(11) DEFAULT NULL,
  `accum_odo` float DEFAULT NULL,
  `temp` varchar(5) DEFAULT NULL,
  `fuel` int(11) DEFAULT NULL,
   `user_id` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK86uaicb374d362ai0t28hbv1q` (`alertid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `offense` (
  `paying` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `offense_type` bigint(20) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  KEY `FKhq4ae95g4iprx5mv7q1insv0w` (`offense_type`),
  CONSTRAINT `FK2mq5t0cwnbed3i2e5q65v524` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`),
  CONSTRAINT `FKhq4ae95g4iprx5mv7q1insv0w` FOREIGN KEY (`offense_type`) REFERENCES `type` (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `oil_change` (
  `air_f` bit(1) DEFAULT NULL,
  `engine_oil` bit(1) DEFAULT NULL,
  `fuel_f` bit(1) DEFAULT NULL,
  `oil_f` bit(1) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `separator_f` bit(1) DEFAULT NULL,
  `type_air_f` bigint(20) DEFAULT NULL,
  `type_engine_oil` bigint(20) DEFAULT NULL,
  `type_fuel_f` bigint(20) DEFAULT NULL,
  `type_oil_f` bigint(20) DEFAULT NULL,
  `type_separator_f` bigint(20) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `odometre` double DEFAULT NULL,
  `air_conditioning_filter` bit(1) DEFAULT NULL,
  `oil_mark` varchar(255) DEFAULT NULL,
  `seal` bit(1) DEFAULT NULL,
  `overtaking` int(11) DEFAULT NULL,
  `brake_oil` BIT(1) DEFAULT NULL,
  `type_brake_oil` BIGINT(20) DEFAULT NULL,
  `cooling_oil` BIT(1) DEFAULT NULL,
  `type_cooling_oil` BIGINT(20) DEFAULT NULL,
  `windshield_washer_oil` BIT(1) DEFAULT NULL,
  `type_windshield_washer_oil` BIGINT(20) DEFAULT NULL,
  `hydraulic_suspension_oil` BIT(1) DEFAULT NULL,
  `type_hydraulic_suspension_oil` BIGINT(20) DEFAULT NULL,
  `gearbox_oil` BIT(1) DEFAULT NULL,
  `type_gearbox_oil` BIGINT(20) DEFAULT NULL,
  `direction_oil` BIT(1) DEFAULT NULL,
  `type_direction_oil` BIGINT(20) DEFAULT NULL,
  `reference` BIGINT(20) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  `lubrication` BIT(1) DEFAULT NULL,
  `type_lubrication` BIGINT(20) DEFAULT NULL,
   `hydrolique_oil` bit(1) DEFAULT NULL,
  `type_hydrolique_oil` bigint(20) DEFAULT NULL,
  `boite_oil` bit(1) DEFAULT NULL,
  `type_boite_oil` bigint(20) DEFAULT NULL,
  `pont_oil` bit(1) DEFAULT NULL,
  `type_pont_oil` bigint(20) DEFAULT NULL,
  `graissage_oil` bit(1) DEFAULT NULL,
  `type_graissage_oil` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `FKl9a0v6vhemybvnt4e42bs675e` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `oil_change_pont_boite` (
  `quantity` bigint(20) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `odometre` bigint(20) DEFAULT NULL,
  `hydrolique_oil` bit(1) DEFAULT NULL,
  `type_hydrolique_oil` bigint(20) DEFAULT NULL,
  `reference` bigint(20) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  `boite_oil` bit(1) DEFAULT NULL,
  `type_boite_oil` bigint(20) DEFAULT NULL,
  `pont_oil` bit(1) DEFAULT NULL,
  `type_pont_oil` bigint(20) DEFAULT NULL,
  `graissage_oil` bit(1) DEFAULT NULL,
  `type_graissage_oil` bigint(20) DEFAULT NULL,
  `direction_oil` bit(1) DEFAULT NULL,
  `type_direction_oil` bigint(20) DEFAULT NULL,
  `workforce` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `fk_oil_change_pont_boite_1` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `operator` (
  `operator_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`operator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `other_maintenance` (
  `date_buy` datetime DEFAULT NULL,
  `km_montage` bigint(20) NOT NULL DEFAULT '0',
  `mark` varchar(255) DEFAULT NULL,
  `max_km` bigint(20) NOT NULL DEFAULT '0',
  `number` bigint(20) DEFAULT NULL,
  `serie_number` varchar(255) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `odometre` double DEFAULT '0',
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `fk_other_1` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `path` (
  `device_id` bigint(20) NOT NULL,
  `begin_path_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_path_time` timestamp NULL DEFAULT NULL,
  `begin_path_latitude` double DEFAULT NULL,
  `begin_path_longitude` double DEFAULT NULL,
  `start_adress` text,
  `end_path_latitude` double DEFAULT NULL,
  `end_path_longitude` double DEFAULT NULL,
  `end_adress` text,
  `max_speed` int(11) DEFAULT NULL,
  `path_duration` bigint(20) DEFAULT '0',
  `distance_driven` double DEFAULT NULL,
  `fuel_used` double NOT NULL DEFAULT '0',
  `start_fuel` double NOT NULL DEFAULT '0',
  `end_fuel` double NOT NULL DEFAULT '0',
  `start_odo` double NOT NULL DEFAULT '0',
  `end_odo` double NOT NULL DEFAULT '0',
 `start_tfu` double NOT NULL DEFAULT '0',
 `end_tfu` double NOT NULL DEFAULT '0',
  `StartAdress_according_poi` text,
  `EndAdress_according_poi` text,
  PRIMARY KEY (`device_id`,`begin_path_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `path_config` (
  `device_id` int(11) NOT NULL,
  `path_min_speed` int(11) DEFAULT '5',
  `path_min_sec` int(11) DEFAULT '20',
  `stop_min_sec` int(11) DEFAULT '120',
  `pause_min_sec` int(11) DEFAULT '60',
  `distance_min_meter` double DEFAULT '10',
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `payment_type` (
  `id_payment_type` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_payment_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `poi_group_poi` (
  `group_poi_id` bigint(20) NOT NULL,
  `point_interest_id` bigint(20) NOT NULL,
  KEY `fk_poi_group_poi_1_idx` (`group_poi_id`),
  KEY `fk_poi_group_poi_2_idx` (`point_interest_id`),
  CONSTRAINT `fk_poi_group_poi_1` FOREIGN KEY (`group_poi_id`) REFERENCES `group_poi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_poi_group_poi_2` FOREIGN KEY (`point_interest_id`) REFERENCES `point_interest` (`point_interest_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `point_interest` (
  `point_interest_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `image_uri` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `niveau_zoom` bigint(20) DEFAULT NULL,
  `ray` int(11) DEFAULT NULL,
  `zoom_level` bigint(20) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `hash` longtext,
  `type` varchar(255) DEFAULT NULL,
 `code_client` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`point_interest_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `point_client` (
  `point_client_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `hash` longtext,
  `image_uri` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `polygone_color` varchar(20) DEFAULT NULL,
  `ray` int(11) DEFAULT NULL,
  `type_poi` int(11) DEFAULT NULL,
  `zoom_level` bigint(20) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `code_client` varchar(45) DEFAULT NULL,
   `actif` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`point_client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `point_client_group_point_client` (
  `group_point_client_id` bigint(20) NOT NULL,
  `point_client_id` bigint(20) NOT NULL,
  KEY `fk_point_client_group_point_client_1_idx` (`group_point_client_id`),
  CONSTRAINT `fk_point_client_group_poi_1` FOREIGN KEY (`group_point_client_id`) REFERENCES `group_point_client` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `provider` (
  `id_provider` bigint(20) NOT NULL AUTO_INCREMENT,
  `adresse` varchar(50) DEFAULT NULL,
  `codetva` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_provider`),
  UNIQUE KEY `UK_7pvp08p4hu0e5k4452khlhv78` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `program` (
  `id_program` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_program`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

CREATE TABLE `real_time_dev` (
  `deviceid` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(20) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `fuel` int(11) DEFAULT NULL,
  `ignition` bit(1) DEFAULT NULL,
  `mems_x` int(11) DEFAULT NULL,
  `mems_y` int(11) DEFAULT NULL,
  `mems_z` int(11) DEFAULT NULL,
  `power` int(11) DEFAULT NULL,
  `record_time` datetime DEFAULT NULL,
  `sat_in_view` int(11) DEFAULT NULL,
  `send_flag` int(11) DEFAULT NULL,
  `speed` int(11) DEFAULT NULL,
  `temperature` varchar(10) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `validity` bit(1) DEFAULT NULL,
  `signal` varchar(45) DEFAULT NULL,
  `rotation_angle` int(11) DEFAULT NULL,
  `rpm` int(11) NOT NULL DEFAULT '0',
  `fuel_rate` double NOT NULL DEFAULT '0',
  `tfu` double NOT NULL DEFAULT '0',
  `odo` double NOT NULL DEFAULT '0',
  `temp_engine` int(11) NOT NULL DEFAULT '0',
  `accum_odo` float NOT NULL DEFAULT '0',
  `last_raw_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `di1`bit(1) NOT NULL DEFAULT b'0',
  `di2`bit(1) NOT NULL DEFAULT b'0',
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`deviceid`)
) ENGINE=MEMORY  DEFAULT CHARSET=utf8;

CREATE TABLE `rep_activity` (
  `device_id` int(11) NOT NULL,
  `activity_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat` double NOT NULL DEFAULT '0',
  `activity_start_lon` double NOT NULL DEFAULT '0',
  `start_adress` text CHARACTER SET utf8,
  `activity_start_odo` double NOT NULL DEFAULT '0',
  `activity_start_tfu` double NOT NULL DEFAULT '0',
  `activity_end_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_end_lat` double NOT NULL DEFAULT '0',
  `activity_end_lon` double NOT NULL DEFAULT '0',
  `end_adress` text CHARACTER SET utf8,
  `activity_end_odo` double NOT NULL DEFAULT '0',
  `activity_end_tfu` double NOT NULL DEFAULT '0',
  `activity_start_fuel_level` int(11) NOT NULL DEFAULT '0',
  `activity_end_fuel_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`,`activity_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `rep_activity_daily` (
  `device_id` int(11) NOT NULL,
  `activity_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat` double DEFAULT '0',
  `activity_start_lon` double DEFAULT '0',
  `start_adress` text CHARACTER SET utf8,
  `activity_start_odo` double NOT NULL DEFAULT '0',
  `activity_start_tfu` double NOT NULL DEFAULT '0',
  `activity_end_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_end_lat` double DEFAULT '0',
  `activity_end_lon` double DEFAULT '0',
  `end_adress` text CHARACTER SET utf8,
  `activity_end_odo` double NOT NULL DEFAULT '0',
  `activity_end_tfu` double NOT NULL DEFAULT '0',
  `working_time` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`,`activity_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `rep_activity_daily_path` (
  `device_id` int(11) NOT NULL,
  `activity_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `activity_start_lat` double DEFAULT '0',
  `activity_start_lon` double DEFAULT '0',
  `start_adress` text CHARACTER SET utf8,
  `activity_start_odo` double NOT NULL DEFAULT '0',
  `activity_start_tfu` double NOT NULL DEFAULT '0',
  `activity_end_time` timestamp NULL DEFAULT '2017-01-01 00:00:00',
  `activity_end_lat` double DEFAULT '0',
  `activity_end_lon` double DEFAULT '0',
  `end_adress` text CHARACTER SET utf8,
  `activity_end_odo` double NOT NULL DEFAULT '0',
  `activity_end_tfu` double NOT NULL DEFAULT '0',
  `working_time` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`,`activity_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `reparation` (
  `odometre` bigint(20) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `reparation_type_id` bigint(20) DEFAULT NULL,
  `next_forecast_odometer` bigint(20) DEFAULT NULL,
  `odometer_repair` bigint(20) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `num_facture` varchar(45) DEFAULT NULL,
  `total_cost` bigint(20) DEFAULT NULL,
  `days` int DEFAULT NULL,
  `mark` varchar(45) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  `total_ttc` double DEFAULT '0',
  `total_tva` double DEFAULT '0',
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `FKcplenxtp421b7e3yooah4ude1` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `reparation_detail` (
  `id_rep` bigint(20) NOT NULL AUTO_INCREMENT,
  `reparation_type_id` bigint(20) DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `quantite` bigint(20) DEFAULT NULL,
  `next_rep_odometre` bigint(20) DEFAULT NULL,
  `labor_cost` double DEFAULT NULL,
  `next_rep_time` datetime DEFAULT NULL,
  `reparation_id` bigint(20) DEFAULT NULL,
  `label` varchar(225) DEFAULT NULL,
  `remise` double DEFAULT NULL,
  `tva` int(11) DEFAULT NULL,
  `reference` varchar(225) DEFAULT NULL,
  `prix_ht` double DEFAULT NULL,
  `max_km` double DEFAULT '0',
  `is_referenced` tinyint(1) DEFAULT '0',
  `mark` varchar(45) DEFAULT NULL,
  `prix_htva` double DEFAULT '0',
  `prix_ttc` double DEFAULT '0',
  PRIMARY KEY (`id_rep`),
  KEY `fk_reparation_type_idx` (`reparation_type_id`),
  KEY `fk_reparation_detail_reparation_idx` (`reparation_id`),
  CONSTRAINT `fk_reparation_detail_reparation` FOREIGN KEY (`reparation_id`) REFERENCES `reparation` (`id_maintenance`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reparation_type` FOREIGN KEY (`reparation_type_id`) REFERENCES `type` (`id_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `rep_fuel_variation` (
  `id_device` int(11) NOT NULL,
  `type` enum('APPRO','THIEF') NOT NULL DEFAULT 'APPRO',
  `consume_start_fuel_level` double DEFAULT '0',
  `consume_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `consume_start_odo` double DEFAULT '0',
  `consume_start_tfu` double DEFAULT '0',
  `appro_start_fuel_level` double DEFAULT '0',
  `appro_start_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `appro_start_odo` double DEFAULT '0',
  `appro_start_tfu` double DEFAULT '0',
  `appro_end_fuel_level` double DEFAULT '0',
  `appro_end_time` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `appro_end_odo` double DEFAULT '0',
  `appro_end_tfu` double DEFAULT '0',
  `verify` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_device`,`appro_start_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `report_auto` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `period` varchar(255) DEFAULT NULL,
  `emails` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `payload` TEXT DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `signin_url` varchar(255) DEFAULT NULL,
`authority` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `representing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `telephone` int(11) DEFAULT NULL,
  `cin` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


CREATE TABLE `rep_overspeed` (
  `device_id` bigint(20) NOT NULL,
  `begin_path_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_path_time` timestamp NULL DEFAULT NULL,
  `begin_path_latitude` double DEFAULT NULL,
  `begin_path_longitude` double DEFAULT NULL,
  `start_adress` text,
  `end_path_latitude` double DEFAULT NULL,
  `end_path_longitude` double DEFAULT NULL,
  `end_adress` text,
  `max_speed` int(11) DEFAULT NULL,
  `path_duration` bigint(20) DEFAULT '0',
  `distance_driven` double DEFAULT NULL,
  `fuel_used` double NOT NULL DEFAULT '0',
  `start_fuel` double NOT NULL DEFAULT '0',
  `end_fuel` double NOT NULL DEFAULT '0',
  `start_odo` double NOT NULL DEFAULT '0',
  `end_odo` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`device_id`,`begin_path_time`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `route_leg` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sequence` int(11) DEFAULT NULL,
  `travel_distance` double DEFAULT NULL,
  `travel_duration` double DEFAULT NULL,
  `fk_mission` bigint(20) DEFAULT NULL,
  `fk_point_of_interest_source` bigint(20) DEFAULT NULL,
  `fk_point_of_interest_target` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9u2kmbptp2k0a9gr36rwu6tjq` (`fk_point_of_interest_source`),
  KEY `FK5ruu5y9cov0lpl4vipvum43a9` (`fk_point_of_interest_target`),
  KEY `fk_route_leg_1_idx` (`fk_mission`),
  CONSTRAINT `fk_route_leg_1` FOREIGN KEY (`fk_mission`) REFERENCES `mission` (`mission_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_route_leg_2` FOREIGN KEY (`fk_point_of_interest_source`) REFERENCES `point_interest` (`point_interest_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_route_leg_3` FOREIGN KEY (`fk_point_of_interest_target`) REFERENCES `point_interest` (`point_interest_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `running_document` (
  `end_date` datetime DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `id_document` bigint(20) NOT NULL,
  `begin_date` date DEFAULT NULL,
  `maturity_amount` double DEFAULT NULL,
  `ref` varchar(255) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `id_doc_merge` bigint(20) DEFAULT NULL,
 `observation` varchar(45) DEFAULT NULL,
 `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_document`),
  KEY `fk_running_document_1_idx` (`id_doc_merge`),
  CONSTRAINT `FK2k8uj22hbx2bca4yvgusrbmm0` FOREIGN KEY (`id_document`) REFERENCES `document` (`id_document`),
  CONSTRAINT `fk_running_document_1` FOREIGN KEY (`id_doc_merge`) REFERENCES `running_document_merge` (`id_doc_merge`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `running_document_merge` (
  `id_doc_merge` bigint(20) NOT NULL,
  `begin_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `cost_total` double DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_doc_merge`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `sav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NULL DEFAULT NULL,
  `vehicule_id` bigint(20) DEFAULT NULL,
  `etat` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `description` varchar(45) DEFAULT NULL,
  `technician` varchar(45) DEFAULT NULL,
  `date_repare` timestamp NULL DEFAULT NULL,
  `matricule` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

CREATE TABLE `sinistre` (
  `description_damage` varchar(255) DEFAULT NULL,
  `place` varchar(255) DEFAULT NULL,
  `refund` bigint(20) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `cabinet_expertise` bigint(20) DEFAULT NULL,
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  KEY `FKeilc805w5w929y5p17ano6woi` (`cabinet_expertise`),
  CONSTRAINT `FKeilc805w5w929y5p17ano6woi` FOREIGN KEY (`cabinet_expertise`) REFERENCES `type` (`id_type`),
  CONSTRAINT `FKfokbsoaujxfqycd2adig4671x` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `stop` (
  `stop_start` datetime NOT NULL,
  `device_id` int(11) NOT NULL,
  `stop_end` datetime DEFAULT NULL,
  `in_path` varchar(255) DEFAULT NULL,
  `stop_duration` bigint(20) DEFAULT '0',
  `stop_lat` double DEFAULT NULL,
  `stop_long` double DEFAULT NULL,
  `stop_adress` varchar(1000) DEFAULT NULL,
  `Adress_according_poi` text,
  PRIMARY KEY (`stop_start`,`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `suggestion` (
  `suggestion_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `value` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`suggestion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `time_plan` (
  `id_time_plan` int(11) NOT NULL AUTO_INCREMENT,
  `id` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `start` timestamp NULL DEFAULT NULL,
  `end` timestamp NULL DEFAULT NULL,
  `background` varchar(45) DEFAULT NULL,
  `about` varchar(45) DEFAULT NULL,
  `recurrence_rule` varchar(45) DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_time_plan`),
  KEY `fk_time_plan_1_idx` (`program_id`),
  CONSTRAINT `fk_time_plan_1` FOREIGN KEY (`program_id`) REFERENCES `program` (`id_program`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tire_maintenance` (
  `date_buy` datetime DEFAULT NULL,
  `km_montage` bigint(20) DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `max_km` bigint(20) DEFAULT NULL,
  `number` bigint(20) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `serie_number` varchar(255) DEFAULT NULL,
  `id_maintenance` bigint(20) NOT NULL,
  `balancing` bit(1) DEFAULT NULL,
  `calibration` bit(1) DEFAULT NULL,
  `parallelism` bit(1) DEFAULT NULL,
  `pneu` BIT(1) DEFAULT NULL,
  `type_pneu` VARCHAR(45) DEFAULT NULL,
  `odometre` double NOT NULL DEFAULT '0',
  `reference_unique` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_maintenance`),
  CONSTRAINT `FKl6qdexteub4jxwi9402jhp26c` FOREIGN KEY (`id_maintenance`) REFERENCES `maintenance` (`id_maintenance`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `type` (
  `id_type` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `typical_path` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin_path_time` datetime DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createdat` datetime DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `lastpasswordresetdate` datetime DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  UNIQUE KEY `UK_sb8bbouer5wak8vyiiy4pf2bx` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

CREATE TABLE `useralert` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` bigint(20) DEFAULT NULL,
  `group_id` bigint(20) DEFAULT NULL,
  `name` enum('SPEED','INGNITION_V0','ZONE','POI','TOW','NO_POI','DRIVING_QUALITY','CHARGER','DOC','WEEK_CONTROL','VIDANGE','TIRE','BATTERY','BRAKE','SIPHONAGE') CHARACTER SET utf8 DEFAULT NULL,
  `type` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `time_to_realert` int(11) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `email_mode` tinyint(1) NOT NULL DEFAULT '0',
  `emails` varchar(1000) COLLATE utf8_unicode_ci DEFAULT '',
   `email_text` text COLLATE utf8_unicode_ci,
  `title` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notification_text` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sms` varchar(255) COLLATE utf8_unicode_ci DEFAULT '',
   `sms_mode` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `DEVICE_INDEX` (`device_id`),
  KEY `GROUP_INDEX` (`group_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `user_authority` (
  `user_id` bigint(20) NOT NULL,
  `authority_id` bigint(20) NOT NULL,
  KEY `FKgvxjs381k6f48d5d2yi11uh89` (`authority_id`),
  KEY `FKpqlsjpkybgos9w2svcri7j8xy` (`user_id`),
  CONSTRAINT `FKgvxjs381k6f48d5d2yi11uh89` FOREIGN KEY (`authority_id`) REFERENCES `authority` (`id`),
  CONSTRAINT `FKpqlsjpkybgos9w2svcri7j8xy` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user_group` (
  `user_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  KEY `FKb0xhtx49263tfllv47gq517as` (`group_id`),
  KEY `FK1c1dsw3q36679vaiqwvtv36a6` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user_group_poi` (
  `user_id` bigint(20) NOT NULL,
  `group_poi_id` bigint(20) NOT NULL,
  KEY `index1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `user_vehicule` (
  `user_id` bigint(20) NOT NULL,
  `vehicule_id` bigint(20) NOT NULL,
  KEY `FK8lo1dg4cru47g3d6e3cn4bfuq` (`vehicule_id`),
  KEY `FKfti4gsuf6vcu63h35ay8xeslq` (`user_id`),
  CONSTRAINT `FK8lo1dg4cru47g3d6e3cn4bfuq` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicule` (`vehicule_id`),
  CONSTRAINT `FKfti4gsuf6vcu63h35ay8xeslq` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `vehicule` (
  `vehicule_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mark` varchar(255) DEFAULT NULL,
  `matricule` varchar(50) NOT NULL,
  `path_gray_card` varchar(255) DEFAULT NULL,
  `alias` varchar(50) DEFAULT NULL,
  `emails` varchar(255) DEFAULT NULL,
  `max_speed` bigint(20) DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  `model` varchar(50) DEFAULT NULL,
  `odo_offset` double NOT NULL DEFAULT '0',
  `last_odo_update` timestamp NOT NULL DEFAULT '2017-01-01 00:00:00',
  `circulation_at` datetime DEFAULT NULL,
  `sub_mark` varchar(50) DEFAULT NULL,
  `have_avatar` tinyint(1) DEFAULT '0',
  `have_gray_card` tinyint(1) DEFAULT '0',
  `capacity` varchar(100) DEFAULT NULL,
  `category` varchar(45) DEFAULT NULL,
  `num_carte_fuel` varchar(45) DEFAULT NULL,
  `mark_boitier` varchar(45) DEFAULT NULL,
  `date_install_boitier` datetime NULL DEFAULT NULL,
  `program_id` int(11) DEFAULT NULL,
  `option_arret` tinyint(1) DEFAULT '0',
  `rattachement` varchar(50) NOT NULL,
  `status` tinyint(1) DEFAULT '0',
    `date_garantie` varchar(50) NOT NULL,
  PRIMARY KEY (`vehicule_id`),
  UNIQUE KEY `UK_b8fyikkqe8bflk5vh1r7mdlda` (`matricule`),
  KEY `fk_vehicule_1_idx` (`program_id`),
  CONSTRAINT `fk_vehicule_1` FOREIGN KEY (`program_id`) REFERENCES `program` (`id_program`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE `vehicule_mission` (
  `mission_id` bigint(20) NOT NULL,
  `vehicule_id` bigint(20) NOT NULL,
  PRIMARY KEY (`mission_id`,`vehicule_id`),
  KEY `fk_vehicule_mission_2_idx` (`vehicule_id`),
  CONSTRAINT `fk_vehicule_mission_1` FOREIGN KEY (`mission_id`) REFERENCES `mission` (`mission_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vehicule_mission_2` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicule` (`vehicule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `vehicule_navette` (
  `navette_id` int(11) NOT NULL,
  `vehicule_id` bigint(20) NOT NULL,
  PRIMARY KEY (`navette_id`,`vehicule_id`),
  KEY `fk_vehicule_navette_2_idx` (`vehicule_id`),
  CONSTRAINT `fk_vehicule_navette_1` FOREIGN KEY (`navette_id`) REFERENCES `navette` (`navette_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_vehicule_navette_2` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicule` (`vehicule_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `vehicule_groupe` (
  `groupe_id` bigint(20) NOT NULL,
  `vehicule_id` bigint(20) NOT NULL,
  PRIMARY KEY (`groupe_id`,`vehicule_id`),
  KEY `FKgvi1s0hdm04m77wxs0q0dg9sa` (`vehicule_id`),
  CONSTRAINT `FKgvi1s0hdm04m77wxs0q0dg9sa` FOREIGN KEY (`vehicule_id`) REFERENCES `vehicule` (`vehicule_id`),
  CONSTRAINT `FKhuni467k9bicyoty344j3tute` FOREIGN KEY (`groupe_id`) REFERENCES `groupe` (`groupe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `web_setting` (
  `id_web_setting` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_web_setting` varchar(255) DEFAULT NULL,
  `device_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_web_setting`),
  KEY `FKnhhdjd587qk4k6aacrln9o4tb` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `authority`(`label`,`name`) VALUES ('Temps réel', 'ROLE_RT'),('Historique', 'ROLE_PATH'),('Gestion des données', 'ROLE_DM'),('Gestion du parc', 'ROLE_PM'),('Gestion des alertes', 'ROLE_AM'),('Générateur de rappoo
rts', 'ROLE_RM'),('Vue sur l\'ensemble', 'ROLE_OV'),('Arret/Lancement moteur','ROLE_ENGINE_CONTROLE'),('Gestion du parc/Document circulation','ROLE_PM_CD'),('Gestion du parc/Carburant','ROLE_PM_FUEL'),('Gestion du parc/Vidange','ROLE_PM_VIDANGE'),('Gestion du parc/Entretien','ROLE_PM_MAINTENANCE'),('Gestion du parc/Pertes','ROLE_PM_LOSSES'),('Gestion du parc/Locations','ROLE_PM_LEASING'),('Gestion du parc/Divers','ROLE_PM_VARIOUS'),('Gestion du parc/TDB','ROLE_PM_DASHBOARD'),('Gestion du parc/Charge conducteur','ROLE_PM_DC'),('Rapport Graphiques', 'ROLE_SM'), ('Ajouter Modifier POI', 'ROLE_POI'),('Gestion de mission', 'ROLE_MISSION');

insert into `groupe` (`groupe_id`,`url_image`,`nom`) values (null,null,'tout');

insert ignore into `device_carb` (`device_id`) select  `id_device` from `device`;

insert ignore into `device_mems`  (`device_id`) select  `id_device` from `device`;

insert ignore into `device_opt`  (`device_id`) select  `id_device` from `device`;

insert ignore into `device_temp`  (`device_id`) select  `id_device` from `device`;

INSERT IGNORE INTO `alertconfigurationoperator` VALUES (1,'='),(2,'!='),(3,'>='),(4,'<='),(5,'Between'),(6,'Not between'),(7,'OUT'),(8,'IN'); 

/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
